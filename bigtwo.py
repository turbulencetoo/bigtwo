import socket
import sys

import bigtwo_common as com

class BigTwo:
    def __init__(self, remote_ip_addr, remote_port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((remote_ip_addr, remote_port))
        self.hand = None
        self.last_play = None

    def pass_turn(self):
        packet = com.Packet()
        packet.type = com.PASS
        self.sock.send(packet.dumpb())

    def play_cards(self, hand):
        packet = com.Packet()
        packet.type = com.PLAY
        packet.cards = hand
        self.sock.send(packet.dumpb())

    def run(self):
        print("Hello and welcome to BigTwo")
        print("To enter cards in this game, separate them with a '{}'".format(com.CARD_SEP))
        print("Example: 8c{}8s".format(com.CARD_SEP))
        while True:
            packet = com.Packet()
            packet.load_sock(self.sock)

            if packet.type == com.HAND:
                assert self.hand == None  # if server sends a hand ours shouldnt be init yet
                self.hand = packet.cards
            elif packet.type == com.PLAY:
                print("Your opponent played: {}".format(packet.cards))
                self.last_play = packet.cards
            elif packet.type == com.PASS:
                print("Your opponent elected to PASS")
                self.last_play = None
            elif packet.type == com.CARD:
                print("You drew the {}".format(packet.cards[0]))
                self.hand.append(packet.cards[0])
            elif packet.type == com.GO:
                self.take_turn()

    def take_turn(self):
        print("Your turn. Your hand is:")
        com.big_print(self.hand)
        if not self.last_play:
            print("You may lead a 1-, 2-, or 5-card hand.")
        else:
            print("You must either play a {}-card hand stronger than {}, or PASS"
                  .format(len(self.last_play), self.last_play))

        input_is_valid = False
        while not input_is_valid:
            user_input = input("Your option: ")
            if not user_input:
                continue
            elif user_input[0].upper() == "P":
                self.pass_turn()
                input_is_valid = True
            else:
                # parse input as a list of cards
                cards = []
                if not all(len(card_str) == 2 for card_str in user_input.split(com.CARD_SEP)):
                    print("INVALID INPUT")
                    continue
                for card_str in user_input.split(com.CARD_SEP):
                    cards.append(com.Card(com.STRINGS_TO_RANKS[card_str[0].upper()], 
                                          com.STRINGS_TO_SUITS[card_str[1]]))
                if not all(card in self.hand for card in cards):
                    print("NOT LEGAL")
                    continue
                if (self.last_play != None and 
                        not com.hand_is_greater(cards, self.last_play)):
                    print("NOT BIGGER")
                    continue

                input_is_valid = True
                self.play_cards(cards)
                for card in cards:
                    self.hand.remove(card)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("usage: python3 {} ipaddr port".format(sys.argv[0]))
        sys.exit(1)
    bt = BigTwo(sys.argv[1], int(sys.argv[2]))
    bt.run()

