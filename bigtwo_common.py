import random

INIT  = 1  #
HAND  = 2  # Server sends full hand to client
PLAY  = 3  # client or server is communicating most recent play
PASS  = 4  # client or server indicating most recnt play is a pass
CARD  = 5  # server sending a drawn card to client
GO    = 6  # server saying "your turn"

ITEM_SEP = b";"
CARD_INTERAL_SEP = "."
CARD_SEP = ","
TERMINATOR = b"|"

class Packet:
    def __init__(self):
        self.type = 0
        self.cards = None
        self.msg = None
    def dumpb(self):
        out = []
        out.append(str(self.type))
        if self.cards:
            cards_out = []
            for card in self.cards:
                cards_out.append(str(card.rank) + CARD_INTERAL_SEP + str(card.suit))
            out.append(CARD_SEP.join(cards_out))
        else:
            out.append("")
        if self.msg:
            out.append(self.msg)
        else:
            out.append("")
        return ITEM_SEP.join(s.encode("utf-8") for s in out) + TERMINATOR

    def loadb(self, bstring):
        bstring = bstring.rstrip(TERMINATOR)
        split_bstring = bstring.split(ITEM_SEP)
        type_string, cards_string, msg_string = [bs.decode("utf-8") for bs in split_bstring]

        self.type = int(type_string)

        if cards_string:
            self.cards = []
            for card_string in cards_string.split(CARD_SEP):
                card_rank_string, card_suit_string = card_string.split(CARD_INTERAL_SEP)
                self.cards.append(Card(int(card_rank_string), int(card_suit_string)))
        
        if msg_string:
            self.msg = msg_string

    def load_sock(self, socket):
        barray = []
        while True:
            data = socket.recv(1)
            barray.append(data)
            if data == TERMINATOR:
                break
        self.loadb(b"".join(barray))

STRAIGHT = 1000
FLUSH = 2000
FULL = 3000
QUAD = 4000
STRFLUSH = 5000

SUITS = [1, 2, 3, 4]
SUIT_STRINGS = {1: "c", 2: "d", 3: "h", 4: "s"}
STRINGS_TO_SUITS = {"c": 1, "d": 2, "h": 3, "s": 4}
RANKS = [3, 4, 5, 6, 7, 8, 9, 10 ,11, 12 ,13, 14, 15]
RANK_STRINGS = { 3: "3", 4: "4", 5: "5", 6: "6", 7: "7", 8: "8", 9: "9", 10: "T", 11: "J", 12: "Q", 13: "K", 14: "A", 15: "2"}
STRINGS_TO_RANKS = {"3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "T": 10, "J": 11, "Q": 12, "K": 13, "A": 14, "2": 15
}
def hand_is_greater(hand1, hand2):
    length = len(hand1)
    assert length == len(hand2)

    if length == 5:
        return rank5(hand1) > rank5(hand2)
    elif length == 2:
        assert hand1[0].rank == hand1[1].rank
        assert hand2[0].rank == hand2[1].rank

        return max(hand1) > max(hand2)
    elif length == 1:
        return hand1[0] > hand2[0]

def rank5(hand):
    '''
    Returns a unique number for a 5-card hand that indicates its strength.
    Given two five card hands A, and B. rank5(A) > rank5(B) IFF A is a better 
     big two hand than B.
    '''
    return max(rankStraight5(hand), rankFlush5(hand), rankFull5(hand), rankQuad5(hand), rankStrFlush5(hand))

def rankStraight5(hand):
    cards = sorted(hand)
    prev = cards[0]
    for card in cards[1:]:
        if card.rank != prev.rank + 1:
            return 0
        prev = card
    return STRAIGHT + 10 * cards[-1].rank + cards[-1].suit

def rankFlush5(hand):
    if not all(card.suit == hand[0].suit for card in hand):
        return 0
    top = max(hand)
    return FLUSH + 100 * top.suit + top.rank

def rankFull5(hand):
    counts = {}
    for card in hand:
        if card.rank not in counts:
            counts[card.rank] = 0
        counts[card.rank] += 1
    two_of = None
    three_of = None
    for rank in counts:
        if counts[rank] == 3:
            three_of = rank
        if counts[rank] == 2:
            two_of = rank
    if two_of and three_of:
        return FULL + three_of
    else:
        return 0

def rankQuad5(hand):
    counts = {}
    for card in hand:
        if card.rank not in counts:
            counts[card.rank] = 0
        counts[card.rank] += 1
    four_of = None
    for rank in counts:
        if counts[rank] == 4:
            four_of = rank
    if four_of:
        return QUAD + four_of
    else:
        return 0

def rankStrFlush5(hand):
    if rankStraight5(hand) and rankFlush5(hand):
        return STRFLUSH + 100 * max(hand).suit + max(hand).rank
    else:
        return 0

def big_print(hand):
    print("#" * 40)
    for suit in SUITS:
        for rank in RANKS:
            card = Card(rank, suit)
            if card in hand:
                print(card, end="")
            else:
                print("  ", end="")
            print(" ", end="")
        print()
    print("#" * 40)


class Deck(list):
    def __init__(self):
        list.__init__(self)
        self.extend([Card(rank, suit) for rank in RANKS for suit in SUITS])
        random.shuffle(self)

    def deal(self, number=None):
        if number:
            return [self.pop() for i in range(number)]
        else:
            return self.pop()

class Card:
    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit
    def __str__(self):
        return repr(self)
    def __repr__(self):
        return RANK_STRINGS[self.rank] + SUIT_STRINGS[self.suit]

    def __lt__(self, other):
        return (self.rank < other.rank or
                (self.rank == other.rank and
                 self.suit < other.suit))
    def __le__(self, other):
        return self < other or self == other
    def __eq__(self, other):
        return self.rank == other.rank and self.suit == other.suit
    def __ne__(self, other):
        return not (self == other)
    def __gt__(self, other):
        return (self.rank > other.rank or
                (self.rank == other.rank and
                 self.suit > other.suit))
    def __ge__(self, other):
        return self > other or self == other

if __name__ == "__main__":
    d = Deck()
    hand = d.deal(17)
    big_print(hand)