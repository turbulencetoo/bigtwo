#!/usr/bin/env python3
'''
bigtwo_server.py

A simple attempt a server that runs Big Two. Adapted from my chatter server.
'''
import sys
import socket
import os
import threading

import bigtwo_common as com

MAX_COMMUNICATIONS = 2

class BigTwoServer:
    def __init__(self, local_ip_addr, local_port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((local_ip_addr, local_port))
        self.clients = []

    def get_clients(self):
        while len(self.clients) < MAX_COMMUNICATIONS:
            self.sock.listen(MAX_COMMUNICATIONS)
            conn, addr = self.sock.accept()
            self.clients.append(conn)

    def send_hand(self, client, hand):
        packet = com.Packet()
        packet.type = com.HAND
        packet.cards = hand
        client.send(packet.dumpb())

    def send_go(self, client):
        packet = com.Packet()
        packet.type = com.GO
        client.send(packet.dumpb())

    def send_card(self, client, cards):
        '''cards will be a list of length one with one card'''
        packet = com.Packet()
        packet.type = com.CARD
        packet.cards = cards
        client.send(packet.dumpb())

    def relay_packet(self, client, packet):
        client.send(packet.dumpb())


    def run(self):
        deck = com.Deck()
        hand0 = deck.deal(17)
        hand1 = deck.deal(17)
        if min(hand0) < min(hand1):
            active_player, other_player = self.clients[0], self.clients[1]
        else:
            active_player, other_player = self.clients[1], self.clients[0]
        self.send_hand(self.clients[0], hand0)
        self.send_hand(self.clients[1], hand1)

        self.send_go(active_player)
        while True:
            packet = com.Packet()
            packet.load_sock(active_player)
            if packet.type == com.PASS:
                card = deck.deal(1)
                self.send_card(active_player, card)
            self.relay_packet(other_player, packet)
            active_player, other_player = other_player, active_player  # E-Z swap in python
            self.send_go(active_player)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("usage: python3 {} ipaddr port".format(sys.argv[0]))
        sys.exit(1)
    bts = BigTwoServer(sys.argv[1], int(sys.argv[2]))
    bts.get_clients()
    print("{} clients are connected.".format(MAX_COMMUNICATIONS))
    bts.run()